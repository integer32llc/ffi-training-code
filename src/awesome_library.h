// Returns the version of the library
int awesome_version(void);

// A user-presentable string of features. The caller should not free
// this string.
char *awesome_features_enabled(void);

// A string to greet the user. The caller must free this string.
char *awesome_greeting(int yelling);

// An opaque type
typedef struct awesome_type awesome_type_t;

// Construct the type
awesome_type_t *awesome_new(void);

// Free the type
void awesome_free(awesome_type_t *);

// Returns a reference to a single value inside the type
int *awesome_value(awesome_type_t *);

// Returns a reference to all the values inside the type
int *awesome_values(awesome_type_t *, int *length);
