mod awesome {
    mod ffi {
        use libc::c_int;

        extern "C" {
            pub fn awesome_version() -> c_int;
        }
    }

    // There's not much we need to do for simple numeric primitives.
    pub fn version() -> i32 {
        unsafe { ffi::awesome_version() }
    }
}

fn main() {
    println!("Library version: {}", awesome::version());
}
