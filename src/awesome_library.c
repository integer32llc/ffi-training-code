#include <string.h>
#include <stdlib.h>
#include "awesome_library.h"

int awesome_version(void) {
  return 42;
}

static char *FEATURES = "portability, speed";

char *awesome_features_enabled(void) {
  return FEATURES;
}

char *awesome_greeting(int yelling) {
  if (yelling) {
    return strdup("HELLO");
  } else {
    return strdup("hello");
  }
}

static int LENGTH = 10;

// An opaque type
struct awesome_type {
  int *values;
};

awesome_type_t *awesome_new(void) {
  awesome_type_t *a = malloc(sizeof(awesome_type_t));
  a->values = malloc(10 * sizeof(int));
  for (int i = 0; i < LENGTH; i++) {
    a->values[i] = i;
  }
  return a;
}

// Free the type
void awesome_free(awesome_type_t *a) {
  free(a->values);
  free(a);
}

// Returns a reference into the type
int *awesome_value(awesome_type_t *a) {
  return &a->values[5];
}

// Returns a reference to all the types
int *awesome_values(awesome_type_t *a, int *length) {
  *length = LENGTH;
  return a->values;
}
