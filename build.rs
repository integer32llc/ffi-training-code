fn main() {
    cc::Build::new()
        .file("src/awesome_library.c")
        // For Windows
        .define("_CRT_NONSTDC_NO_DEPRECATE", "1")
        .warnings_into_errors(true)
        .warnings(true)
        .extra_warnings(true)
        .compile("awesome_library");
}
